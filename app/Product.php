<?php

namespace App;

class Product extends Model
{

    public function track() {
        $this->stock->each->track();
    }

    public function inStock() {
        return $this->stock()->whereInStock(true)->exists();
    }

    public function stock() {
        return $this->hasMany(Stock::class);
    }
}
